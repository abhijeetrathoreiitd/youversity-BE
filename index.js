/**
 * Module dependencies
 */

var done = false;
var express = require('express'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    http = require('http'),
    path = require('path'),
    routes = require('./routes'),
    api = require('./routes/api'),
    fs = require('node-fs');
var AWS = require('aws-sdk');

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var multer = require('multer');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
});

var upload = multer({storage: storage})


AWS.config.update({
    accessKeyId: 'AKIAJLU25P6MIP34PTAQ',
    secretAccessKey: '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'
});


//-----------------Requiring Controllers Begin------------------//
var BookSessionController = require('./controllers/BookSessionController');
var SubscribeTrainingPlanController = require('./controllers/SubscribeTrainingPlanController');
var CalendarController = require('./controllers/CalendarController');
var FeedbackController = require('./controllers/FeedbackController');
var FollowController = require('./controllers/FollowController');
var TrainingPlanCategoryController = require('./controllers/TrainingPlanCategoryController');
var VideoReviewController = require('./controllers/VideoReviewController');
var CommentController = require('./controllers/CommentController');
var NewsFeedController = require('./controllers/NewsFeedController');
var ProfileController = require('./controllers/ProfileController');
var RatingController = require('./controllers/RatingController');
var SportExperienceController = require('./controllers/SportExperienceController');
var ExperienceListController = require('./controllers/ExperienceListController');
var PreExperienceListController = require('./controllers/PreExperienceListController');
var SportsController = require('./controllers/SportsController');
var TagsController = require('./controllers/TagsController');
var TagsLevel1Controller = require('./controllers/TagsLevel1Controller');
var TagsLevel2Controller = require('./controllers/TagsLevel2Controller');
var TagsLevel3Controller = require('./controllers/TagsLevel3Controller');
var UserController = require('./controllers/UserController');
var VideoController = require('./controllers/VideoController');
var PackageController = require('./controllers/PackageController');
var ActivitiesController = require('./controllers/ActivitiesController');
var TrainingPlanController = require('./controllers/TrainingPlanController');
var TournamentController = require('./controllers/TournamentController');
var SubscribeController = require('./controllers/SubscribeController');
var BookmarkController = require('./controllers/BookmarkController');
var AcademyController = require('./controllers/AcademyController');
//-----------------Requiring Controllers End------------------//

// Constants
var DEFAULT_PORT = 8085;
var PORT = process.env.PORT || DEFAULT_PORT;

// App
var app = express();
app.set('views', __dirname + '/views');
//app.set('view engine', 'jade');
app.set('view engine', 'ejs');
app.set("jsonp callback", true);
app.set('jsonp callback name', 'code');
app.use(function (req, res, next) {
    //console.log('%s %s %s', req.method, req.url, req.path);
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    if ('OPTIONS' == req.method) {
        res.sendStatus(200);
    }
    else {
        next();
    }
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(session({
    secret: 'this is a secret key',
    resave: false,
    saveUninitialized: true
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(passport.session());

//user sign-up strategy
passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true // allows us to pass back the entire request to the callback
    },
    UserController.userSignUp
));
//user login strategy
passport.use('local-login', new LocalStrategy(
    {
        usernameField: 'email'
    },
    UserController.userSignIn
));

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});

/**
 * Routes
 */

app.options('*', function (req, res) {
    res.sendStatus(200);
});

app.post('/user', passport.authenticate('local-signup'), function (req, res) {
    if (req) {
        res.json({"user": req.user});
    }
    else {
        console.log("error")
    }
});
app.post('/user/fbSignUp', UserController.fbSignUp);


app.post('/login', passport.authenticate('local-login'), function (req, res) {
    if (req) {
        res.json({"user": req.user});
    }
    else {
        console.log("error")
    }
});


app.post('/uploadThumbnail', upload.single('file'), function (req, res, next) {
    var imageName = new Date().getTime();
    var path2 = 'public/uploads/' + imageName + path.extname(req.file.originalname)
    fs.rename(req.file.path, path2, function (data) {
        console.dir(data);
    })
    res.send(imageName + path.extname(req.file.originalname));
});


app.post('/uploadAudioReview', upload.single('file'), function (req, res, next) {

    //var s3 = new AWS.S3();
    var params = {Bucket: 'config-bucket-570600719000', Key: 'audio/demo' + new Date().toISOString() +path.extname(req.file.originalname)};
    var body = fs.createReadStream(req.file.path);
    var s3obj = new AWS.S3({params: params});
    s3obj.upload({Body: body}).on('httpUploadProgress', function (evt) {
        console.log(evt);
    }).send(function (err, data) {

        if (err) {
            res.send({error: err});
        }

        else {
            console.log('aws response')
            console.dir(data)
            res.send(data.Location);
        }
    });

});

app.post('/uploadImage', upload.single('file'), function (req, res, next) {

    //var s3 = new AWS.S3();
    var params = {Bucket: 'config-bucket-570600719000', Key: 'videos/demo' + new Date().toISOString() + '.mp4'};
    console.dir(req.file)
    var body = fs.createReadStream(req.file.path);
    var s3obj = new AWS.S3({params: params});
    s3obj.upload({Body: body}).on('httpUploadProgress', function (evt) {
        console.log(evt);
    }).send(function (err, data) {

        if (err) {
            res.send({error: err});
        }

        else {
            console.log('aws response')
            console.dir(data)
            res.send(data.Location);
        }
    });

});

app.post('/sports', SportsController.addSport);
app.post('/tags', TagsController.addTags);
app.post('/sportExperience', SportExperienceController.addExperience);
app.get('/sports', SportsController.getAllSports);


app.get('/videoStats', UserController.getVideoStats);
app.get('/likeVideo', UserController.updateVideoLikes);
app.get('/dislikeVideo', UserController.updateVideoDislikes);
app.get('/viewVideo', UserController.updateVideoViews);
app.get('/getAllCoachesForReview', UserController.getAllCoachesForReview);
app.post('/userType', UserController.checkUserType);

//VIDEO API's

app.post('/video', VideoController.addVideo);
app.get('/video', VideoController.getUserVideos);
app.get('/getVideoBySportId', VideoController.getVideoBySportId);
app.get('/searchVideoByTitle', VideoController.searchVideoByTitle);


//Follow API's
app.post('/follow', FollowController.addFollowing);
app.get('/follow/:userId', FollowController.getFollowers);
app.get('/unfollow', FollowController.unfollow);


//Subscribe API's
app.post('/subscribe', SubscribeController.addSubscribed);
app.get('/subscribe/:userId', SubscribeController.getSubscribedBy);


//Subscribe API's
app.post('/bookmark', BookmarkController.addBookmark);
app.get('/bookmark/:userId', BookmarkController.getBookmark);

//Review API's
app.post('/submitForReview', VideoReviewController.addReview);
app.post('/videoForReview', VideoReviewController.getVideo);
app.put('/submitReview', VideoReviewController.submitReview);


//Package API's
app.post('/package', PackageController.addPackage);
app.get('/package', PackageController.getPackage);
app.put('/updateVideoInPackage', PackageController.updateVideoInPackage);

//Experience List

app.get('/experienceList', ExperienceListController.get);
app.post('/experienceList', ExperienceListController.set);


//Preexperience List

app.get('/preexperienceList', PreExperienceListController.get);
app.post('/preexperienceList', PreExperienceListController.set);

//NewsFeed API's
app.get('/newsfeed', NewsFeedController.getNewsFeed);


//Find PLAYER COACH PROFILE
app.get('/findperson', SportExperienceController.findperson);



//TRAINING Plan  API's

app.post('/trainingPlanCategory', TrainingPlanCategoryController.addTrainingPlanCategory);
app.get('/trainingPlanCategory', TrainingPlanCategoryController.getTrainingPlanCategoryBySportId);
app.post('/trainingPlan', TrainingPlanController.addTrainingPlan);
app.get('/trainingPlan', TrainingPlanController.getTrainingPlanByCategory);
app.get('/coachTrainingPlan', TrainingPlanController.getCoachTrainingPlan);
app.get('/getUserStats', TrainingPlanController.getUserStats);



//Activity  API's

app.put('/activity', ActivitiesController.updateActivityByCoach);
app.get('/activity', ActivitiesController.getActivityByTrainingPlanId);


//Comment API's

app.post('/comment', CommentController.addComment);
// app.post('/comment',CommentController.getActivityByTrainingPlanId);


//SubscribeTrainingPlan API's

app.post('/myTrainingPlan', SubscribeTrainingPlanController.subscribePlan);
app.get('/myTrainingPlan', SubscribeTrainingPlanController.getPlan);
app.put('/myTrainingPlan', SubscribeTrainingPlanController.updateTrainingPlan);
app.get('/myTrainingPlanUpdateStatus', SubscribeTrainingPlanController.updateTrainingPlanStatus);
app.get('/rateMyTrainingPlan', SubscribeTrainingPlanController.rateMyTrainingPlan);



//TagsControllers API's
app.post('/tagsLevel1', TagsLevel1Controller.addTagsLevel1);
app.post('/tagsLevel2', TagsLevel2Controller.addTagsLevel2);
app.post('/tagsLevel3', TagsLevel3Controller.addTagsLevel3);
app.get('/tagsLevel1', TagsLevel1Controller.getTagsLevel1);
app.get('/tagsLevel2', TagsLevel2Controller.getTagsLevel2);
app.get('/tagsLevel3', TagsLevel3Controller.getTagsLevel3);
app.get('/getAllLevelTags', TagsLevel1Controller.getAllLevelTags);

//Tournament API's
app.post('/tournament', TournamentController.addTournament);


//Tournament API's
app.post('/academy', AcademyController.addAcademy);



app.post('/appReview', UserController.appReview);
app.get('/deleteabhijeetiitd', UserController.deleteabhijeetiitd);


// serve index and view partials
app.get('/', routes.index);
app.get('/partials/:name', routes.partials);
// JSON API
app.get('/api/name', api.name);
app.get('*', routes.index);
app.listen(PORT);
console.log('Running on http://localhost:' + PORT);
