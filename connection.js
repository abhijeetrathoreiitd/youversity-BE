/**
 * Created by anubhavshrimal on 29/7/16.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
mongoose.connect('mongodb://localhost:27017/mongooseSparup');

var dbmongoose = mongoose.connection;
dbmongoose.on('error', console.error.bind(console, 'connection error:'));
dbmongoose.once('open', function (callback) {
});

var deepPopulate = require('mongoose-deep-populate')(mongoose);

module.exports = {
    mongoose: mongoose,
    Schema: Schema,
    deepPopulate: deepPopulate
};