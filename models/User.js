/**
 * Created by anubhavshrimal on 29/7/16.
 */

var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;

var UserSchema = Schema({
        firstName: {type: String, required: true, trim: true,lowercase: true},
        lastName: {type: String, trim: true,lowercase: true},
        photoLink: {type: String, trim: true},
        coverPhotoLink: {type: String, trim: true},
        email: {type: String, required: true, trim: true,lowercase: true},
        phoneNumber: {type: String, trim: true, minlength: 10, maxlength: 10},
        fbUserId: {type: String, trim: true},
        password: {type: String, trim: true},
        verified: {type: Boolean, required: true, default: false, trim: true},
        lastSignIn: {type: Date, default: null, trim: true},
        coach: {type: Boolean, default: null, trim: true},
        player: {type: Boolean, default: null, trim: true},
        videoReviewOffered: {type: Boolean, default: null, trim: true},
        geoLocation: {type: Object, trim: true},
        sportsExperience: [{type: Schema.Types.ObjectId, ref: 'SportExperience'}],
        viewedVideos: [{type: String}],
        likedVideos: [{type: String}],
        dislikedVideos: [{type: String}]
    },
    {
        timestamps: true,
        collection: "User"
    });

UserSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('User', UserSchema);

