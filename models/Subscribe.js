var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;

var SubscribeSchema = Schema({
        user: {type: String, required: true},
        subscribed: [{type: Schema.Types.ObjectId, ref: 'User'}],
        subscribedBy: [{type: Schema.Types.ObjectId, ref: 'User'}],
    },
    {
        timestamps: true,
        collection: "Subscribe"
    });

SubscribeSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('Subscribe', SubscribeSchema);