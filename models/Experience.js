var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;

var ExperienceListSchema = Schema({
        experience: {type: String, required: true, trim: true},
        title: {type: String, required: true, trim: true}
    },
    {
        timestamps: true,
        collection: "ExperienceList"
    });


module.exports = mongoose.model('ExperienceList', ExperienceListSchema);

