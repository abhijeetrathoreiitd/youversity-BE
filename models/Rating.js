/**
 * Created by anubhavshrimal on 29/7/16.
 */

var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;
//Rating is for COACH ONLY, NOT FOR VIDEOS
var RatingSchema = Schema({
        refToSportExperience: {type: Schema.Types.ObjectId, ref: 'SportExperience'},
        rating: {type: Number, default: 0, trim: true},
        ratingBy: [Schema({
            refToUser: {type: Schema.Types.ObjectId, ref: 'User'},
            ratingValue: {type: Number, default: 0, trim: true}
        })]

    },
    {
        timestamps: true,
        collection: "Rating"
    });

RatingSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('Rating', RatingSchema);
