/**
 * Created by anubhavshrimal on 29/7/16.
 */

var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;

var NewsFeedSchema = Schema({
        newsFeedCode: {type: Number, required: true, trim: true},
        videoId: {type: Schema.Types.ObjectId, ref: 'Video'},
        trainingPlanId: {type: Schema.Types.ObjectId, ref: 'TrainingPlan'},
        userId: {type: Schema.Types.ObjectId, ref: 'User'}
    },
    {
        timestamps: true,
        collection: "NewsFeed"
    });

NewsFeedSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('NewsFeed', NewsFeedSchema);