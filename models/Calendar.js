/**
 * Created by anubhavshrimal on 29/7/16.
 */

var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;

var CalendarSchema = Schema({
        tasks: [Schema({
            time: {type: Date, default: null, trim: true},
            task: {type: String, required: true, trim: true}
        })],
        refToUser: {type: Schema.Types.ObjectId, ref: 'User'}
    },
    {
        timestamps: true,
        collection: "Calendar"
    });

CalendarSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('Calendar', CalendarSchema);