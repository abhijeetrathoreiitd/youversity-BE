/**
 * Created by anubhavshrimal on 02/08/16.
 */

var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;

var AcademySchema = Schema({
        name: {type: String, required: true, trim: true,lowercase: true},
        location: {type: String, required: true, trim: true,lowercase: true},
        sportId: {type: String, required: true, trim: true},
        userId: {type: String, required: true, trim: true},
        from: {type: String, trim: true},
        to: {type: Date, trim: true},
        current: {type: Boolean, trim: true}
    },
    {
        timestamps: true,
        collection: "Academy"
    });

AcademySchema.plugin(deepPopulate, {});

module.exports = mongoose.model('Academy', AcademySchema);

