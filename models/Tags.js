var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;

var TagsSchema = Schema({
        tagName: {type: String, required: true, trim: true},
        sportId: {type: String, required: true, trim: true}
    },
    {
        timestamps: true,
        collection: "Tags"
    });

module.exports = mongoose.model('Tags', TagsSchema);