var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;

var TagsLevel2Schema = Schema({
        tagLevel1Id: {type: Schema.Types.ObjectId, required: true, trim: true},
        tagLevel2: {type: String, required: true, trim: true},
    },
    {
        timestamps: true,
        collection: "TagsLevel2"
    });

module.exports = mongoose.model('TagsLevel2', TagsLevel2Schema);