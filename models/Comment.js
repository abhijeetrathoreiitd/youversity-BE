var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;
var CommentSchema = Schema({
        videoId: {type: Schema.Types.ObjectId},
        comment: [Schema({
                comment: {type: String, required: true, trim: true},
                userId: {type: Schema.Types.ObjectId, ref: 'User'}
            },
            {
                timestamps: true

            })]
    },
    {
        timestamps: true,
        collection: "Comment"
    });

CommentSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('Comment', CommentSchema);

