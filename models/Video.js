/**
 * Created by anubhavshrimal on 29/7/16.
 */

var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;

var VideoSchema = Schema({
        title: {type: String, required: true, trim: true},
        description: {type: String, required: true, trim: true},
        url: {type: String, required: true, trim: true},
        sport: {type: String, required: true, trim: true},
        thumbnail: {type: String, required: true, trim: true},
        paidVideo: {type: Boolean, required: true, trim: true},//paid unpaid,
        duration: {type: Number, trim: true},//store seconds duration
        subscribers: [{type: Schema.Types.ObjectId, ref: 'User'}],
        refToFeedback: [{type: Schema.Types.ObjectId, ref: 'Feedback'}],
        videoBy: {type: Schema.Types.ObjectId, ref: 'User'},
        review: {type: Schema.Types.ObjectId, ref: 'VideoReview'},
        accountType: {type: String, required: true, trim: true},//accountType of User(Player/Coach)
        numberOfViews: {type: Number, default: 0, trim: true},
        likes: {type: Number, min: 0, default: 0, trim: true},
        dislikes: {type: Number, min: 0, default: 0, trim: true},
        refToPackage: [{type: Schema.Types.ObjectId, ref: 'Package'}],
        tagsLevel1: [{type: Schema.Types.ObjectId}],
        tagsLevel2: [{type: Schema.Types.ObjectId}],
        tagsLevel3: [{type: Schema.Types.ObjectId}],
        comment: [{type: Schema.Types.ObjectId, ref: 'Comment'}]
    },
    {

        timestamps: true,
        collection: "Video"
    });

VideoSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('Video', VideoSchema);

