var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;

var PreExperienceListSchemaList = Schema({
        preExperience: {type: String, required: true, trim: true}
    },
    {
        timestamps: true,
        collection: "PreExperienceList"
    });


module.exports = mongoose.model('PreExperienceList', PreExperienceListSchemaList);









