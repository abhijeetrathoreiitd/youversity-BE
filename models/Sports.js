/**
 * Created by anubhavshrimal on 29/7/16.
 */

var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;

var SportsSchema = Schema({
        sportName: {type: String, required: true, trim: true},
        refToTags: [{type: Schema.Types.ObjectId, ref: 'Tags'}],
    },
    {
        timestamps: true,
        collection: "Sports"
    });

SportsSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('Sports', SportsSchema);