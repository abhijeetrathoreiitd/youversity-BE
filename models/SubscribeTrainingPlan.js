var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;

var SubscribeTrainingPlanSchema = Schema({
        userId: {type: Schema.Types.ObjectId, ref: 'User'},
        planList: [Schema({
            trainingPlanId: {type: Schema.Types.ObjectId, ref: 'TrainingPlan'},
            startDate: {type: Date},
            status: {type: String},
            activity: [Schema({
                activityId: {type: Schema.Types.ObjectId, ref: 'Activities'},
                completed: {type: Boolean, default: false},
                day: {type: Number, default: false},
                
            })],
            sport: {type: Schema.Types.ObjectId, ref: 'Sports'},
            rating:{type: Number},
            feedback: {type: String},
        }, {
            timestamps: true,         
        })]
    },
    {
        timestamps: true,
        collection: "SubscribeTrainingPlan"
    });

SubscribeTrainingPlanSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('SubscribeTrainingPlan', SubscribeTrainingPlanSchema);


