var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;

var TrainingPlanCategorySchema = Schema({
        // category name may repeat for different sportID
        category: {type: String, required: true, trim: true,lowercase: true},
        trainingPlan: [{type: Schema.Types.ObjectId, ref: 'TrainingPlan'}],
        sportId: {type: String, required: true, trim: true},
    },
    {
        timestamps: true,
        collection: "TrainingPlanCategory"
    });

module.exports = mongoose.model('TrainingPlanCategory', TrainingPlanCategorySchema);