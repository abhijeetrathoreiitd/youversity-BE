var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;

var TagsLevel3Schema = Schema({
        tagLevel2Id: {type: Schema.Types.ObjectId, required: true, trim: true},
        tagLevel3: {type: String, required: true, trim: true},
    },
    {
        
        timestamps: true,
        collection: "TagsLevel3"
    });

module.exports = mongoose.model('TagsLevel3', TagsLevel3Schema);