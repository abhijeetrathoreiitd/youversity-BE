/**
 * Created by anubhavshrimal on 29/7/16.
 */

var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;

var FeedbackSchema = Schema({
        refToUser: {type: Schema.Types.ObjectId, ref: 'User'},
        content: {type: String, required: true, trim: true},
        refToVideo: {type: Schema.Types.ObjectId, ref: 'Video'}
    },
    {
        timestamps: true,
        collection: "Feedback"
    });

FeedbackSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('Feedback', FeedbackSchema);