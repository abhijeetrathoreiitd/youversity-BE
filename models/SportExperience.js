/**
 * Created by anubhavshrimal on 29/7/16.
 */

var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;

var SportExperienceSchema = Schema({

        userId: {type: String, required: true, trim: true},
        sportId: {type: String, required: true, trim: true},
        previousExperience: {type: String, trim: true},
        experience: {type: String, trim: true},
        refToTournament: [{type: Schema.Types.ObjectId, ref: 'Tournament'}],
        refToAcademy: [{type: Schema.Types.ObjectId, ref: 'Academy'}],
        brief: {type: String, trim: true},
        level: {type: String, trim: true},
        player: {type: Boolean, trim: true},
        refToRating: [{type: Schema.Types.ObjectId, ref: 'Rating'}]//pass sportExperienceId
    },
    {
        timestamps: true,
        collection: "SportExperience"
    });

module.exports = mongoose.model('SportExperience', SportExperienceSchema);