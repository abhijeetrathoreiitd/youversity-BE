/**
 * Created by Guru on 25/08/2016.
 */
var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;

var PackageSchema = Schema({
        sportId: {type: String, required: true, trim: true},
        public: {type: String, trim: true},
        packageBy: {type: String, trim: true},
        packageName: {type: String, trim: true,lowercase: true},
        tags: [{type: String, trim: true}],
        noOfSubscriber: {type: Number, trim: true, default: 0},
        cost: {type: Number, trim: true},
        paid: {type: Boolean, trim: true},
        refToVideos: [{type: Schema.Types.ObjectId, ref: 'Video'}]
    },
    {
        timestamps: true,
        collection: "Package"
    });

PackageSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('Package', PackageSchema);
