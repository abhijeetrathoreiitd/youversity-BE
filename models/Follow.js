var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;

var FollowSchema = Schema({
        user: {type: String, required: true},
        following: [{type: Schema.Types.ObjectId, ref: 'User'}],
        follower: [{type: Schema.Types.ObjectId, ref: 'User'}],
    },
    {
        timestamps: true,
        collection: "Follow"
    });

FollowSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('Follow', FollowSchema);