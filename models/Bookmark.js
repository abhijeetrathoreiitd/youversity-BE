var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;
var BookmarkSchema = Schema({
        userId: {type: String, required: true, trim: true},
        bookmarkedVideos: [{type: String}]
    },
    {
        timestamps: true,
        collection: "Bookmark"
    });

BookmarkSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('Bookmark', BookmarkSchema);

