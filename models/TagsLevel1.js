var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;

var TagsLevel1Schema = Schema({
        tagLevel1: {type: String, required: true, trim: true},
        sportId: {type: Schema.Types.ObjectId, required: true, trim: true}
    },
    {
        timestamps: true,
        collection: "TagsLevel1"
    });

module.exports = mongoose.model('TagsLevel1', TagsLevel1Schema);