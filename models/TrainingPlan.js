var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;

var TrainingPlanSchema = Schema({
        trainingName: {type: 'String', trim: true,lowercase: true},
        trainingBy: {type: Schema.Types.ObjectId, ref: 'User'},
        duration: {type: 'Number', default: 0, trim: true},
        rating: {type: 'Number', trim: true},
        cost: {type: 'Number', default: 0, trim: true},
        feedback: [
            {
                feedback: {type: 'String', trim: true},
                rating: {type: 'Number', trim: true},
                user: {type: Schema.Types.ObjectId, ref: 'User'},
            }
        ],
        noOfSubscriber: {type: 'Number', default: 0, trim: true},
        category: [{type: Schema.Types.ObjectId, ref: 'TrainingPlanCategory'}],
        sport: {type: Schema.Types.ObjectId, ref: 'Sports'},
        description: {type: 'String', trim: true,lowercase: true},
        level: {type: 'String', trim: true},
        tagsLevel1: [{type: Schema.Types.ObjectId}],
        tagsLevel2: [{type: Schema.Types.ObjectId}],
        tagsLevel3: [{type: Schema.Types.ObjectId}],

    },
    {
        timestamps: true,
        collection: "TrainingPlan"
    });

TrainingPlanSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('TrainingPlan', TrainingPlanSchema);