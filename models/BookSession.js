/**
 * Created by anubhavshrimal on 29/7/16.
 */
var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;

var BookSessionSchema = Schema({
        refToUser_Coach: {type: Schema.Types.ObjectId, ref: 'User'},
        refToUser_Player: {type: Schema.Types.ObjectId, ref: 'User'},
        meetingDate: {type: Date, default: Date.now, trim: true}
    },
    {
        timestamps: true,
        collection: "BookSession"
    });

BookSessionSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('BookSession', BookSessionSchema);
