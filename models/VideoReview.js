var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;

var VideoReviewSchema = Schema({
        videoBy: {type: String, required: true, trim: true},
        videoId: {type: String, required: true, trim: true},
        review: [Schema({
            coachId: {type: Schema.Types.ObjectId, ref: 'User'},
            review: {type: String, trim: true},
            audioUrl: {type: String, trim: true}
        })]
    },
    {
        timestamps: true,
        collection: "VideoReview"
    });

VideoReviewSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('VideoReview', VideoReviewSchema);