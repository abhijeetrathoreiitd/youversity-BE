var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;

var ActivitiesSchema = Schema({
        day: {type: Number, required: true, trim: true},
        trainingPlanId: {type: Schema.Types.ObjectId, ref: 'TrainingPlan'},
        sport: {type: Schema.Types.ObjectId, ref: 'Sports'},
        activities: [Schema({
            activityName: {type: String, required: true, trim: true,lowercase: true},
            description: {type: String, required: true, trim: true,lowercase: true},
            unit1: {type: String,trim: true},
            unit2: {type: String,trim: true},
            round: {type: Number,trim: true},//stores the no of times an activity is performed
            videoId: {type: Schema.Types.ObjectId, ref: 'Video'}
        })]
    },
    {
        timestamps: true,
        collection: "Activities"
    });

ActivitiesSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('Activities', ActivitiesSchema);

