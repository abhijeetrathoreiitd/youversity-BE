/**
 * Created by anubhavshrimal on 29/7/16.
 */

var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;

var ProfileSchema = Schema({
        role: {type: String, required: true, trim: true},
        level: {type: String, trim: true},
        videoReviewOffered: {type: Boolean, default: false, trim: true},
        briefDesc: {type: String, trim: true},
        refToSportExperience: [{type: Schema.Types.ObjectId, ref: 'SportExperience'}],
        refToUser: {type: Schema.Types.ObjectId, ref: 'User'},
        refToSports: [{type: Schema.Types.ObjectId, ref: 'Sports'}]
    },
    {
        timestamps: true,
        collection: "Profile"
    });

ProfileSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('Profile', ProfileSchema);
