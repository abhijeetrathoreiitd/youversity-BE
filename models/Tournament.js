/**
 * Created by youstart on 26-Aug-16.
 */


var connection = require('../connection');
var mongoose = connection.mongoose;
var Schema = connection.Schema;
var deepPopulate = connection.deepPopulate;

var TournamentSchema = Schema({
        name: {type: String, required: true, trim: true,lowercase: true},
        location: {type: String, required: true, trim: true,lowercase: true},
        sportId: {type: String, required: true, trim: true},
        userId: {type: String, required: true, trim: true},
        team: {type: String, trim: true,lowercase: true},
        date: {type: Date, trim: true},
        role: {type: String, trim: true,lowercase: true}
    },
    {
        timestamps: true,
        collection: "Tournament"
    });

TournamentSchema.plugin(deepPopulate, {});

module.exports = mongoose.model('Tournament', TournamentSchema);

