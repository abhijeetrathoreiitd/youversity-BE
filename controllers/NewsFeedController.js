var NewsFeed = require('../models/NewsFeed');

var async = require('async')

module.exports = {
    addNewsFeed: addNewsFeed,
    getNewsFeed: getNewsFeed
};

function addNewsFeed(newsFeedObject) {

    var newsFeedEntry = new NewsFeed(newsFeedObject);
    newsFeedEntry.save(function (err, result) {
        if (err)
            console.dir(err);
        else
            console.log('news feed saved');

    })

}

function getNewsFeed(req, res) {

    var limit = 10;
    var newFetchTime = req.query.newFetchTime;
    var oldFetchTime = req.query.oldFetchTime;
    var fetchcase = parseInt(req.query.fetchcase);// 1.for new news fetch 2. for old new fetch
    var query = {};
    //2. old news fetch
    switch (fetchcase) {
        case 1:
            if (typeof newFetchTime != 'undefined' && newFetchTime.length != 0) {
                query = {
                    createdAt: {$gt: newFetchTime}
                }
            }
            console.log('case1')
            break;

        case 2:
            if (typeof oldFetchTime != 'undefined' && oldFetchTime.length != 0) {
                query = {
                    createdAt: {$lt: oldFetchTime}
                }
            }

            console.log('case2')
            break;
    }
    NewsFeed.find(query).sort({createdAt: -1}).limit(limit).deepPopulate('videoId userId trainingPlanId').lean().exec(function (err, result) {

        var loadMoreNewsFeed;
        if (result.length > 0) {
            loadMoreNewsFeed = true;
        }
        else {
            loadMoreNewsFeed = false;
        }
        res.json({newsFeed: result, lastFetchTime: new Date(), loadMoreNewsFeed: loadMoreNewsFeed});
    })


}


function getNewsFeed(req, res) {

    var limit = 10;
    var newFetchTime = req.query.newFetchTime;
    var userId = req.query.userId;
    var oldFetchTime = req.query.oldFetchTime;
    var fetchcase = parseInt(req.query.fetchcase);// 1.for new news fetch 2. for old new fetch
    var query = {};
    //2. old news fetch
    switch (fetchcase) {
        case 1:
            if (typeof newFetchTime != 'undefined' && newFetchTime.length != 0) {
                query = {
                    createdAt: {$gt: newFetchTime}
                }
            }
            console.log('case1');
            break;

        case 2:
            if (typeof oldFetchTime != 'undefined' && oldFetchTime.length != 0) {
                query = {
                    createdAt: {$lt: oldFetchTime}
                }
            }

            console.log('case2')
            break;

        case 3:
            if (typeof userId != 'undefined' && userId.length != 0) {


                if (typeof oldFetchTime != 'undefined' && oldFetchTime.length != 0) {
                    console.log('CASE A');
                    query = {

                        userId: userId,
                        createdAt: {$lt: oldFetchTime}
                    }
                }
                else {
                    query = {

                        userId: userId
                    }
                }
            }

            console.log('case2')
            break;
    }
    NewsFeed.find(query).sort({createdAt: -1}).limit(limit).deepPopulate('videoId userId trainingPlanId').lean().exec(function (err, result) {

        var loadMoreNewsFeed;
        if (result.length > 0) {
            loadMoreNewsFeed = true;
        }
        else {
            loadMoreNewsFeed = false;
        }
        res.json({newsFeed: result, lastFetchTime: new Date(), loadMoreNewsFeed: loadMoreNewsFeed});
    })


}