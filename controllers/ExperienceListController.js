var Experience = require('../models/Experience');

module.exports = {
    get: get,
    set:set
};



function set(req, res) {
    var queryObj=req.body;
    console.dir(queryObj);
    var experience=new Experience(queryObj);
    experience.save(function(err,result)
    {
        console.log(result);
        res.json(result);
    })
}

function get(req, res) {
    Experience.find().lean().exec(function(err,result)
    {
        console.log(result);
        res.json(result);
    })
}
