var TrainingPlanCategory = require('../models/TrainingPlanCategory');
var TrainingPlan = require('../models/TrainingPlan');
var Activities = require('../models/Activities');
var Tournament = require('../models/Tournament');
var Academy = require('../models/Academy');
var async = require('async')
var _ = require('underscore');
var NewsFeedController = require('.././controllers/NewsFeedController');
var SubscribeTrainingPlan = require('../models/SubscribeTrainingPlan');
var Follow = require('../models/Follow');


module.exports = {
    addTrainingPlan: addTrainingPlan,
    getTrainingPlanByCategory: getTrainingPlanByCategory,
    getCoachTrainingPlan: getCoachTrainingPlan,
    getUserStats: getUserStats
};

function addTrainingPlan(req, res) {
    var savePlan = req.body.savePlan;
    console.dir(savePlan);
    var savedPlan;
    var trainingPlanId;
    async.series([
        function (callback) {

            var trainingPlan = new TrainingPlan(savePlan);
            trainingPlan.save(function (err, result) {
                if (result) {
                    trainingPlanId = result._id;
                    savedPlan = result;
                }
                callback();

            })

        },
        function (callback) {


            var activities = [];//makes the list of blank activity according to no of days associated with this training plan. Does not any activity yet.
            for (var i = 0; i < savePlan.duration * 7; i++) {
                activities[i] = {
                    day: i + 1,
                    trainingPlanId: trainingPlanId,
                    sport: savePlan.sport._id,
                    activities: []
                }
            }
            Activities.insertMany(activities, function (err, result) {
                callback();
            })

        },
        function () {

            TrainingPlanCategory.update({_id: {$in: savePlan.category}},
                {$push: {trainingPlan: trainingPlanId}}, {multi: true}, function (err, result) {
                    NewsFeedController.addNewsFeed({
                        userId: savePlan.trainingBy,
                        trainingPlanId: trainingPlanId._id,
                        newsFeedCode: 10
                    });
                    res.json(savedPlan);
                })

        }])

}

function getTrainingPlanByCategory(req, res) {
    var categoryId = req.query.categoryId;
    var sport = req.query.sport;
    TrainingPlan.find({
        category: categoryId,
        sport: sport
    }).deepPopulate('trainingBy').lean().exec(function (err, result) {
        res.json(result);
    })

}

function getCoachTrainingPlan(req, res) {
    var coachId = req.query.coachId;
    TrainingPlan.find({trainingBy: coachId}).deepPopulate('trainingBy sport').lean().exec(function (err, result) {
        res.json(result);
    })
}

function getUserStats(req, res) {
    var coachId = req.query.coachId;
    var followerCount,followingCount,currentlyTraining;
    var trainingPlanIdList = [];
    var tournamentList=[];
    var academyList=[];
    async.series([function (callback) {
        TrainingPlan.find({trainingBy: coachId}).lean().exec(function (err, result) {
            trainingPlanIdList = _.map(result, '_id');
            callback();
        })
    }, function (callback) {

        SubscribeTrainingPlan
            .find({'planList.trainingPlanId': {$in: trainingPlanIdList}, 'planList.status': 'started'})
            .lean().exec(function (err, result) {
            currentlyTraining= result.length;
            callback();
        })

    }, function (callback) {

        Follow
            .findOne({'user':coachId})
            .lean().exec(function (err, result) {
            followerCount= result.follower.length;
            followingCount= result.following.length;
            // res.json({currentlyTraining:currentlyTraining,followerCount:followerCount,followingCount:followingCount})
            callback();
        })

    }, function (callback) {

        Tournament
            .find({'userId':coachId})
            .lean().exec(function (err, result) {
            tournamentList=result;
            callback();
        })

    }, function () {

        Academy
            .find({'userId':coachId})
            .lean().exec(function (err, result) {
            academyList=result;
            console.log('FINAL RESULT:')
            res.json({currentlyTraining:currentlyTraining,
                followerCount:followerCount,
                followingCount:followingCount,
                tournamentList:tournamentList,
                academyList:academyList});
        })

    }])
}