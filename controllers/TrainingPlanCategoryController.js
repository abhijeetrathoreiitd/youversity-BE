var TrainingPlanCategory = require('../models/TrainingPlanCategory');

module.exports = {
    addTrainingPlanCategory: addTrainingPlanCategory,
    getTrainingPlanCategoryBySportId: getTrainingPlanCategoryBySportId
};

function addTrainingPlanCategory(req, res) {
    var category = req.body.category;
    var sportId = req.body.sportId;
    var categoryEntry = new TrainingPlanCategory({
        category: category,
        sportId: sportId
    });
    categoryEntry.save(function (err, tagsDetails) {
        if (err) {
            console.log(err)
        }
        else {
            res.json(tagsDetails);
        }
    })
}

function getTrainingPlanCategoryBySportId(req, res) {

    TrainingPlanCategory.find().lean().exec(function (err, result) {
        res.json(result);
    })

}