var SubscribeTrainingPlan = require('../models/SubscribeTrainingPlan');
var Activities = require('../models/Activities');
var TrainingPlan = require('../models/TrainingPlan');
var _ = require('underscore');
var async = require('async');
var NewsFeedController = require('.././controllers/NewsFeedController');
module.exports = {
    subscribePlan: subscribePlan,
    getPlan: getPlan,
    updateTrainingPlan: updateTrainingPlan,
    updateTrainingPlanStatus: updateTrainingPlanStatus,
    rateMyTrainingPlan: rateMyTrainingPlan
};

function subscribePlan(req, res) {
    var userId = req.body.userId;
    var trainingPlanId = req.body.trainingPlanId;
    var sport = req.body.sport;
    var activityPlanIds = [];
    var planList = {};
    async.series([function (callback) {

        Activities.find({trainingPlanId: trainingPlanId}).lean().exec(function (err, result) {
            if (err)
                res.json(err);
            else {
                _.each(result, function (rslt) {
                    _.each(rslt.activities, function (act) {
                        activityPlanIds.push({activityId: act._id, completed: false, day: rslt.day});
                    })
                })
                callback();
            }
        })
    }, function (callback) {
        planList.trainingPlanId = trainingPlanId;
        // planList.startDate = new Date().toISOString();
        planList.status = 'notStarted';
        planList.activity = activityPlanIds;
        planList.sport = sport;
        callback();
    }, function (callback) {
        SubscribeTrainingPlan.update({userId: userId},
            {$push: {planList: planList}},
            {upsert: true}, function (err, result) {
                // res.json(result);
                callback();
            })

    }, function () {

        TrainingPlan.update({_id: trainingPlanId},
            {$inc: {noOfSubscriber: 1}}, {update: false}, function (err, result) {
                NewsFeedController.addNewsFeed({userId: userId, newsFeedCode: 6, trainingPlanId: trainingPlanId});
                res.json(result);
            })

    }])
}

function getPlan(req, res) {
    var userId = req.query.userId;

    var lastFetchTime = req.query.lastFetchTime;
    var queryObject;

    console.log('lastFetchTime');
    console.log(lastFetchTime);
    if (typeof lastFetchTime != 'undefined')
        queryObject = {
            userId: userId,
            createdAt: {$gte: lastFetchTime}
        }
    else
        queryObject = {
            userId: userId
        }
    SubscribeTrainingPlan.findOne(queryObject).lean().deepPopulate('planList.trainingPlanId.trainingBy').exec(function (err, result) {
        console.log(result);
        res.json({myData: result, lastFetchTime: new Date()});
    })
}


function updateTrainingPlan(req, res) {
    var myPlanId = req.body.myPlanId;
    var activityList = req.body.activityList;

    SubscribeTrainingPlan.update({'planList._id': myPlanId},
        {
            $set: {"planList.$.activity": activityList}
        },
        {upsert: false}, function (err, result) {
            res.json(result);
        })
}

function updateTrainingPlanStatus(req, res) {
    var myPlanId = req.query.myPlanId;
    var planId=req.query.planId;
    var status = req.query.status;
    var updateStartTime = req.query.updateStartTime;
    var userId = req.query.userId;


    var query;
    if (updateStartTime)
        query = {"planList.$.status": status, "planList.$.startDate": new Date().toISOString()}
    else
        query = {"planList.$.status": status}

    SubscribeTrainingPlan.update({'planList._id': myPlanId},
        {
            $set: query
        },
        {upsert: false}, function (err, result) {
            if (result) {
                if (status == 'started')
                    NewsFeedController.addNewsFeed({userId: userId, newsFeedCode: 7, trainingPlanId: planId});
                if (status == 'ended')
                    NewsFeedController.addNewsFeed({userId: userId, newsFeedCode: 8, trainingPlanId: planId});
            }


            res.json(result);
        })
}


function rateMyTrainingPlan(req, res) {

    var rating = parseInt(req.query.rating);
    var userId = req.query.userId;
    var feedback = req.query.feedback;
    var myPlanId = req.query.myPlanId;
    var trainingPlanId = req.query.trainingPlanId;
    var netRating;
    async.series([function (callback) {

        var query = {};
        if (feedback)
            query = {"planList.$.rating": rating, "planList.$.feedback": feedback}
        else
            query = {"planList.$.rating": rating}

        SubscribeTrainingPlan.update({'planList._id': myPlanId},
            {
                $set: query
            },
            {upsert: false}, function (err, result) {

                if (result) {
                    callback();
                }
            })
    }, function (callback) {
        TrainingPlan.update({_id: trainingPlanId},
            {$push: {feedback: {feedback: feedback, rating: rating, userId: userId}}},
            {upsert: false}, function (err, result) {
                console.log(result);
                callback();
            })

    }, function (callback) {
        TrainingPlan.findOne({_id: trainingPlanId}).lean().select('feedback').exec(function (err, result) {

            console.log(_.pluck(result.feedback, 'rating'));
            console.log(_.reduce(_.pluck(result.feedback, 'rating'), function(memo, num){ return memo + num; }, 0))
            console.log((_.pluck(result.feedback, 'rating').length))
            console.log(_.reduce(_.pluck(result.feedback, 'rating'), function(memo, num){ return memo + num; }, 0) / (_.pluck(result.feedback, 'rating').length));
            netRating = _.reduce(_.pluck(result.feedback, 'rating'), function(memo, num){ return memo + num; }, 0) / (_.pluck(result.feedback, 'rating').length);
            if (result)
                callback();

        })

    }, function () {

        console.log(netRating);
        TrainingPlan.update({_id: trainingPlanId}, {$set: {rating: netRating}}, {upsert: false}, function (err, result) {
            console.log(result);
            res.json(result);
        })
    }])
}