/**
 * Created by anubhavshrimal on 29/7/16.
 */

var Sports = require('../models/Sports');

module.exports = {
    addSport: addSport,
    getAllSports: getAllSports
};

function addSport(req, res) {
    var sportName = req.body.sportName;
    var sportsEntry = new Sports({
        sportName: sportName
    });
    sportsEntry.save(function (err, sportDetails) {
        if (err) {

            console.log(err)
        }
        else {
            res.json(sportDetails)
        }
    })
}

function getAllSports(req, res) {

    Sports
        .find()
        .exec(function (err, orders) {
            res.json(orders);
        });
}