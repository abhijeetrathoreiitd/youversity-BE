/**
 * Created by anubhavshrimal on 29/7/16.
 */

var User = require('../models/User');
var Profile = require('../models/Profile');
var Video = require('../models/Video');
var Follow = require('../models/Follow');
var Subscribe = require('../models/Subscribe');
var SportExperience = require('../models/SportExperience');
var bcrypt = require('bcryptjs');
var async = require('async');
var _ = require('underscore');
var NewsFeedController = require('.././controllers/NewsFeedController');
var api_key = 'key-55926f4a678c42180a02d8e58e7e7e82';
var domain = 'pixnary.com';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
// User Sign Up Function Declaration
module.exports = {
    appReview: appReview,
    userSignUp: userSignUp,
    userSignIn: userSignIn,
    getVideoStats: getVideoStats,
    updateVideoLikes: updateVideoLikes,
    updateVideoDislikes: updateVideoDislikes,
    updateVideoViews: updateVideoViews,
    fbSignUp: fbSignUp,
    getAllCoachesForReview: getAllCoachesForReview,
    deleteabhijeetiitd: deleteabhijeetiitd,
    checkUserType: checkUserType
};

function userSignUp(req, email, password, done) {
    var user = req.body.user;//later change it to req.body.user
    User.findOne({email: user.email}, function (err, document) {
        if (!document) {
            bcrypt.genSalt(10, function (err, salt) {
                if (err) return err;
                // hash the password using our new salt
                bcrypt.hash(user.password, salt, function (err, hash) {
                    if (err) return err;
                    var userEntry = new User({
                        firstName: user.firstName,
                        lastName: user.lastName,
                        email: user.email,
                        photoLink: user.photoLink,
                        coverPhotoLink: user.coverPhotoLink,
                        coach: user.coach,
                        player: user.player,
                        videoReviewOffered: user.videoReviewOffered,
                        geoLocation: user.geolocation,
                        password: hash
                    });
                    userEntry.save(function (err, userDetails) {

                        if (err)
                            console.log(err)
                        else {

                            var follow = new Follow({
                                user: userDetails._id
                            });
                            follow.save(function (err, follow) {
                                if (err)
                                    console.log(err)
                                else
                                    return done(null, userDetails);
                            })


                            var follow = new Follow({
                                user: userDetails._id
                            });

                            async.series([
                                function (callback) {
                                    follow.save(function (err, result) {
                                        if (err)
                                            console.log(err)
                                        callback();

                                    })
                                }, function () {
                                    var subscribe = new Subscribe({
                                        user: userDetails._id
                                    });
                                    subscribe.save(function (err, result) {
                                        if (err)
                                            console.log(err)
                                        else {
                                            NewsFeedController.addNewsFeed({userId: userDetails._id, newsFeedCode: 1});
                                            return done(null, userDetails);
                                        }
                                    })
                                }
                            ])


                        }

                    })
                });
            });
        }
        else {
            console.log('user exists');
            return done(null, false);
        }
    });
}

function userSignIn(username, password, done) {

    User.findOne({email: username}).deepPopulate('sportsExperience').exec(
        function (err, document) {
            if (err) {
                console.log(err)
                done(null, false);
            }
            else {
                if (document != null) {
                    bcrypt.compare(password, document.password, function (err, res) {
                        if (res) {
                            console.log("Logged in");
                            return done(null, document);

                        }
                        else {
                            console.log("Invalid Password");
                            return done(null, false);
                        }
                    });
                }
                else {
                    console.log("username does not exists ");
                    //return done(null, false);
                    return done(null, false);
                }
            }

        });

};

function getVideoStats(req, res) {
    var userId = req.query.userId;
    User.findOne({_id: userId})
        .select('viewedVideos likedVideos dislikedVideos')
        .lean()
        .exec(function (err, result) {
            res.json(result);
        })
}


function updateVideoLikes(req, res) {

    var userId = req.query.userId;
    var videoId = req.query.videoId;
    var like = req.query.like;//whether to add or remove item from array
    var dislike = req.query.dislike;
    var queryObject;// used for update the User table

    var queryObject2;// used for update the User table

    if (like == 'true') {
        queryObject = {
            $push: {likedVideos: videoId},
            $pull: {dislikedVideos: videoId}
        }
        NewsFeedController.addNewsFeed({userId: userId, videoId: videoId, newsFeedCode: 4});
        if (dislike == 'true') {
            console.log(1)
            queryObject2 = {
                $inc: {
                    likes: 1,
                    dislikes: -1
                },

            }
        }
        else if (dislike == 'false') {


            queryObject2 = {
                $inc: {likes: 1}
            }
        }
    }
    else if (like == 'false') {
        queryObject = {
            $pull: {likedVideos: videoId}
        }

        queryObject2 = {
            $inc: {likes: -1}
        }

    }


    async.series([
        function (callback) {
            User.update({_id: userId}, queryObject, {upsert: true}, function (err, result) {

                callback();
            })
        }, function () {
            Video.update({_id: videoId}, queryObject2, {upsert: true}, function (err, result) {
                res.json(result)

            })
        }])

}

function updateVideoDislikes(req, res) {

    var userId = req.query.userId;
    var videoId = req.query.videoId;
    var like = req.query.like;//whether to add or remove item from array
    var dislike = req.query.dislike;

    var queryObject;// used for update the User table

    var queryObject2;// used for update the User table

    if (dislike == 'true') {
        queryObject = {
            $push: {dislikedVideos: videoId},
            $pull: {likedVideos: videoId}
        }

        if (like == 'true') {
            queryObject2 = {
                $inc: {
                    dislikes: 1,
                    likes: -1
                },

            }
        }
        else if (like == 'false') {

            queryObject2 = {
                $inc: {dislikes: 1}
            }
        }
    }
    else if (dislike == 'false') {

        queryObject = {
            $pull: {dislikedVideos: videoId}
        }

        queryObject2 = {
            $inc: {dislikes: -1}
        }

    }


    async.series([
        function (callback) {
            User.update({_id: userId}, queryObject, {upsert: true}, function (err, result) {
                console.log(result)
                callback();
            })
        }, function () {
            Video.update({_id: videoId}, queryObject2, {upsert: true}, function (err, result) {
                console.log(result)

                res.json(result)
            })
        }])
}

function updateVideoViews(req, res) {
    var view = req.query.view;
    var userId = req.query.userId;
    var videoId = req.query.videoId;

    if (view == 'true') {

        async.series([
            function (callback) {
                User.update({_id: userId}, {$push: {viewedVideos: videoId}}, {upsert: true}, function (err, result) {
                    console.log(result)
                    callback();
                })
            }, function () {
                Video.update({_id: videoId}, {$inc: {numberOfViews: 1}}, {upsert: true}, function (err, result) {
                    console.log(result)
                    res.json(result)
                })
            }])

    }


}

function fbSignUp(req, res) {

    var user = req.body.user;
    var player = req.body.player;

    console.dir(user)
    User.findOne({fbUserId: user.userId}).deepPopulate('sportsExperience').lean().exec(function (err, document) {
        if (!document) {

            var userEntry = new User({
                firstName: user.name,
                email: user.email,
                fbUserId: user.userId,
                coach: !player,
                player: player,
            });
            userEntry.save(function (err, details) {
                if (err) {
                    res.json({'error': err})
                }
                else {
                    console.log('saved');

                    console.log('firstLogin' + true)
                    res.json({'userData': details, firstLogin: true})
                }
            })
        }
        else {

            if (document.player == player)
                res.json({'userData': document, firstLogin: false});
            else {
                res.json({'userData': 'invalid user', firstLogin: false});
            }
        }
    })

};


function getAllCoachesForReview(req, res) {
    var sportId = req.query.sportId;
    var userIdArray = [];
    console.log(sportId)
    async.series([
        function (callback) {
            SportExperience.find({sportId: sportId}, function (err, result) {
                userIdArray = _.pluck(result, 'userId');
                callback();
            })
        }, function () {
            User.find({_id: {$in: userIdArray}, coach: true}).lean().select('firstName _id')
                .exec(function (err, result) {
                    res.json(result)
                })
        }
    ])

}


function deleteabhijeetiitd(req, res) {

    User.remove({email: 'abhijeetrathoreiitd@gmail.com'}, function (err, result) {
        if (result) {
            res.json('id deleted');
        }
    })

}


function checkUserType(req, res) {
    var email = req.body.email;

    User.findOne({email: email}, function (err, result) {
        var returnResult = {};

        if (result != null && result.fbUserId) {
            returnResult.socialLogin = true;
            returnResult.player = result.player;
        }
        else {

            returnResult.socialLogin = false;
        }
        res.json(returnResult)
    })
}

function appReview(req, res) {



    var message = {
        from: 'Sparup Feedback <no-reply@sparup.com>',
        to: ['abhijeetrathoreiitd@gmail.com','nikhil.punde@gmail.com','vivekrpg@gmail.com'],
        subject: "FeedBack",
        text: 'Feedback: '+req.body.feedback+'\n\n By: '+req.body.name+'\nEmail: '+req.body.email
    };

    mailgun.messages().send(message, function (error, body) {
        console.dir(error)
        console.log('sending mail');
        console.dir(body)
        res.json(body)
    })

}














