/**
 * Created by anubhavshrimal on 29/7/16.
 */

var Video = require('../models/Video');
var Package = require('../models/Package');
var async = require('async');
var NewsFeedController = require('.././controllers/NewsFeedController');


module.exports = {
    addVideo: addVideo,
    getUserVideos: getUserVideos,
    getVideoBySportId: getVideoBySportId,
    searchVideoByTitle: searchVideoByTitle
};


function addVideo(req, res) {
    var videoInfo = req.body.videoInfo;
    console.dir(videoInfo);
    var uploadedVideo = {};
    async.series([
        function (callback) {

            videoInfo.title = videoInfo.title.toLowerCase();
            var video = new Video(videoInfo);
            video.save(function (err, results) {
                if (err) {
                    console.log(err)
                }
                else {
                    uploadedVideo = results;
                    if (videoInfo.package) {
                        callback();
                    }
                    else {

                        NewsFeedController.addNewsFeed({
                            userId: videoInfo.videoBy,
                            videoId: uploadedVideo._id,
                            newsFeedCode: 5
                        });
                        res.json(uploadedVideo);
                    }
                }
            })

        }, function () {
            Package.update({_id: videoInfo.package._id}, {$push: {refToVideos: uploadedVideo._id}}, {upsert: false}, function (err, result) {

                NewsFeedController.addNewsFeed({
                    userId: videoInfo.videoBy,
                    videoId: uploadedVideo._id,
                    newsFeedCode: 5
                });
                res.json(uploadedVideo);
            })

        }])
}

//search videos of particular USER.
function getUserVideos(req, res) {
    var userId = req.query.userId;
    var lastVideoFetchTime = req.query.lastVideoFetchTime;
    var queryObject = {};
    if (lastVideoFetchTime == '') {
        queryObject = {videoBy: userId};
    }
    else {
        queryObject = {
            videoBy: userId,
            $gte: {createdAt: lastVideoFetchTime}
        };
    }
    Video.find(queryObject).deepPopulate('videoBy review.review.coachId comment.comment.userId').lean().exec(function (err, result) {

     

        res.json({lastVideoFetchTime: new Date(), videoList: result});//return the videos all the videos of particular user
    })
}


//search all videos by SPORTID
function getVideoBySportId(req, res) {

    var sportId = req.query.sportId;
    var lastVideoFetchTime = req.query.lastVideoFetchTime;
    var userId = req.query.userId;

    var queryObject = {};
    console.log(sportId)
    console.log(lastVideoFetchTime)
    if (lastVideoFetchTime == '') {
        queryObject = {
            sport: sportId,
            videoBy: {$ne: userId}
        };
    }
    else {
        queryObject = {
            sport: sportId,
            $gte: {createdAt: lastVideoFetchTime},
            videoBy: {$ne: userId}
        };
    }
    Video.find(queryObject).deepPopulate('videoBy review comment.comment.userId').lean().exec(function (err, result) {
        res.json({lastVideoFetchTime: new Date(), videoList: result, sportId: sportId});//return the videos all the videos of particular user
    })
}

function searchVideoByTitle(req, res) {
    var title = req.query.title.toLowerCase();
    var sportId = req.query.sportId;
    Video.find({title: {'$regex': title}, sport: sportId}).lean().exec(function (err, result) {
        console.log(result)
        res.json(result);
    })
}


