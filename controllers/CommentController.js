var Comment = require('../models/Comment');
var Video = require('../models/Video');
var async = require('async');
var NewsFeedController=require('.././controllers/NewsFeedController');

module.exports = {
    addComment: addComment

};

function addComment(req, res) {
    var userId = req.body.userId;
    var comment = req.body.comment;
    var videoId = req.body.videoId;
    var commentId;
    var newRecord = false;

    async.series([function (callback) {

        Comment.findOneAndUpdate({videoId: videoId}, {
            $push: {
                "comment": {
                    "comment": comment,
                    "userId": userId
                }
            }
        }, {upsert: true}, function (err, result) {
            if (result != null) {
                console.log('old post');
                commentId = result._id;
                console.log(commentId);
            }
            else {

                console.log('new post');
                newRecord = true;
            }


            callback();
        })
    }, function (callback) {
        if (newRecord) {
            var commentObject = {
                videoId: videoId,
                "comment": {
                    "comment": comment,
                    "userId": userId
                }
            }

            var newEntry = new Comment(commentObject);
            newEntry.save(function (err, result) {
                if (result) {

                    commentId = result._id;
                    console.log('new post check');
                    console.log(commentId)
                    callback();
                }
            })
        }
        else {
            callback()
        }

    }, function () {
        console.log(commentId);
        Video.update({_id: videoId}, {comment: commentId}, {upsert: true}, function (err, result) {
            NewsFeedController.addNewsFeed({userId:userId,videoId:videoId,newsFeedCode:9});
            res.json(result);
        })
    }])


}