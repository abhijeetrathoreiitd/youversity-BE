var TagsLevel3 = require('../models/TagsLevel3');

module.exports = {
    addTagsLevel3: addTagsLevel3,
    getTagsLevel3:getTagsLevel3
};

function addTagsLevel3(req, res) {
    var tagLevel3 = req.body.tagLevel3;
    var tagLevel2Id = req.body.tagLevel2Id;
    var tagsEntry = new TagsLevel3({
        tagLevel2Id: tagLevel2Id,
        tagLevel3: tagLevel3
    });

    tagsEntry.save(function(err,result)
    {
        console.log(result);
        res.json(result);
    })
    

}


function getTagsLevel3(req,res)
{
    var tagLevel2Id=req.query.tagLevel2Id;
    TagsLevel3.find({tagLevel2Id:tagLevel2Id}).lean().exec(function(err,result)
    {
        res.json(result);
    })
}