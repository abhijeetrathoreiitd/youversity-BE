var Activities = require('../models/Activities');
var async = require('async');
var _ = require('underscore');

module.exports = {
    updateActivityByCoach: updateActivityByCoach,
    getActivityByTrainingPlanId: getActivityByTrainingPlanId
};

function updateActivityByCoach(req, res) {
    var activities = req.body.activities;
    console.dir(activities);
    console.dir(_.pluck(activities, '_id'));

    async.series([function (callback) {
        Activities.remove({_id: { $in:_.pluck(activities, '_id')}}, function (err, result) {
            console.log(err);
            if (result) {
                callback();
            }
        })
    }, function () {
        Activities.insertMany(activities, function (err, result) {
            console.log(err);
            res.json({updated: result});
        })
    }])


}

function getActivityByTrainingPlanId(req, res) {
    var trainingPlanId = req.query.trainingPlanId;
    Activities.find({trainingPlanId: trainingPlanId}).deepPopulate('trainingPlanId activities.videoId sport').lean().exec(function (err, details) {
        res.json(details);
    })
}


//
//
// {
//     "activityDetails":
//     [
//         {
//             "_id": "57f798559993c00c22ec16f9",
//             "trainingPlanId": "57f5fa95c36e258808cc8206",
//             "day": 1,
//             "sport": "57c006e7c6740498040294be",
//             "activities": [{
//                 "activityName": "activity 10","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 8","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 6","description":[ "activity 6 description","activity 6 description-2"]}
//             ]
//         },
//         {
//             "_id": "57f798559993c00c22ec16fa",
//             "trainingPlanId": "57f5fa95c36e258808cc8206",
//             "day": 2,
//             "sport": "57c006e7c6740498040294be",
//             "activities": [{
//                 "activityName": "activity 10","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 8","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 6","description":[ "activity 6 description","activity 6 description-2"]}
//             ]
//         },
//         {
//             "_id": "57f798559993c00c22ec16fb",
//             "trainingPlanId": "57f5fa95c36e258808cc8206",
//             "day": 3,
//             "sport": "57c006e7c6740498040294be",
//             "activities": [{
//                 "activityName": "activity 10","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 8","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 6","description":[ "activity 6 description","activity 6 description-2"]}
//             ]
//         },
//         {
//             "_id": "57f798559993c00c22ec16fc",
//             "trainingPlanId": "57f5fa95c36e258808cc8206",
//             "day": 4,
//             "sport": "57c006e7c6740498040294be",
//             "activities": [{
//                 "activityName": "activity 10","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 8","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 6","description":[ "activity 6 description","activity 6 description-2"]}
//             ]
//         },
//         {
//             "_id": "57f798559993c00c22ec16fd",
//             "trainingPlanId": "57f5fa95c36e258808cc8206",
//             "day": 5,
//             "sport": "57c006e7c6740498040294be",
//             "activities": [{
//                 "activityName": "activity 10","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 8","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 6","description":[ "activity 6 description","activity 6 description-2"]}
//             ]
//         },
//         {
//             "_id": "57f798559993c00c22ec16fe",
//             "trainingPlanId": "57f5fa95c36e258808cc8206",
//             "day": 6,
//             "sport": "57c006e7c6740498040294be",
//             "activities": [{
//                 "activityName": "activity 10","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 8","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 6","description":[ "activity 6 description","activity 6 description-2"]}
//             ]
//         },
//         {
//             "_id": "57f798559993c00c22ec16ff",
//             "trainingPlanId": "57f5fa95c36e258808cc8206",
//             "day": 7,
//             "sport": "57c006e7c6740498040294be",
//             "activities": [{
//                 "activityName": "activity 10","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 8","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 6","description":[ "activity 6 description","activity 6 description-2"]}
//             ]
//         },
//         {
//             "_id": "57f798559993c00c22ec1700",
//             "trainingPlanId": "57f5fa95c36e258808cc8206",
//             "day": 8,
//             "sport": "57c006e7c6740498040294be",
//             "activities": [{
//                 "activityName": "activity 10","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 8","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 6","description":[ "activity 6 description","activity 6 description-2"]}
//             ]
//         },
//         {
//             "_id": "57f798559993c00c22ec1701",
//             "trainingPlanId": "57f5fa95c36e258808cc8206",
//             "day": 9,
//             "sport": "57c006e7c6740498040294be",
//             "activities": [{
//                 "activityName": "activity 10","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 8","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 6","description":[ "activity 6 description","activity 6 description-2"]}
//             ]
//         },
//         {
//             "_id": "57f798559993c00c22ec1702",
//             "trainingPlanId": "57f5fa95c36e258808cc8206",
//             "day": 10,
//             "sport": "57c006e7c6740498040294be",
//
//             "activities": [{
//                 "activityName": "activity 10","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 8","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 6","description":[ "activity 6 description","activity 6 description-2"]}
//             ]
//         },
//         {
//             "_id": "57f798559993c00c22ec1703",
//             "trainingPlanId": "57f5fa95c36e258808cc8206",
//             "day": 11,
//             "sport": "57c006e7c6740498040294be",
//             "activities": [{
//                 "activityName": "activity 10","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 8","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 6","description":[ "activity 6 description","activity 6 description-2"]}
//             ]
//         },
//         {
//             "_id": "57f798559993c00c22ec1704",
//             "trainingPlanId": "57f5fa95c36e258808cc8206",
//             "day": 12,
//             "sport": "57c006e7c6740498040294be",
//             "activities": [{
//                 "activityName": "activity 10","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 8","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 6","description":[ "activity 6 description","activity 6 description-2"]}
//             ]
//         },
//         {
//             "_id": "57f798559993c00c22ec1705",
//             "trainingPlanId": "57f5fa95c36e258808cc8206",
//             "day": 13,
//             "sport": "57c006e7c6740498040294be",
//             "activities": [{
//                 "activityName": "activity 10","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 8","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 6","description":[ "activity 6 description","activity 6 description-2"]}
//             ]
//         },
//         {
//             "_id": "57f798559993c00c22ec1706",
//             "trainingPlanId": "57f5fa95c36e258808cc8206",
//             "day": 14,
//             "sport": "57c006e7c6740498040294be",
//             "activities": [{
//                 "activityName": "activity 10","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 8","description":[ "activity 6 description","activity 6 description-2"]},
//                 {"activityName": "activity 6","description":[ "activity 6 description","activity 6 description-2"]}
//             ]
//         }
//     ]
// }