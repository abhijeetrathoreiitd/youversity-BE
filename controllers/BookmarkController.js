var Bookmark = require('../models/Bookmark');
var async = require('async')

module.exports = {
    addBookmark: addBookmark,
    getBookmark: getBookmark
};

function addBookmark(req, res) {
    var userId = req.body.userId;
    var videoId = req.body.videoId;
    var addBookmark = req.body.addBookmark;
    var queryObject = {};
    if (addBookmark)
        queryObject = {$push: {bookmarkedVideos: videoId}}
    else
        queryObject = {$pull: {bookmarkedVideos: videoId}}

    Bookmark.update({userId: userId}, queryObject, {upsert: true}, function (err, result) {
        console.log('done');
        res.json(result);
    })


}


function getBookmark(req, res) {
    var userId = req.params.userId;
    Bookmark.findOne({userId: userId})
        .lean().select('bookmarkedVideos')
        .exec(function (err, result) {
            var BMV = [];
            if (result) {
                console.log('TEST')
                console.dir(result)
                BMV = result.bookmarkedVideos;
            }

            res.json(BMV);
        })
}