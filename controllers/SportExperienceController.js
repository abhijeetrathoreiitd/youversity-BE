var SportExperience = require('../models/SportExperience');
var Academy = require('../models/Academy');
var Tournament = require('../models/Tournament');
var User = require('../models/User');
var async = require('async')
var _ = require('underscore')

module.exports = {
    addExperience: addExperience,
    findperson: findperson
};




function addExperience(req, res) {
    var experience = req.body.experience;
    var sportExperienceId;
    async.series([
        function (callback) {
            var sportExperience = new SportExperience(experience);
            sportExperience.save(function (err, results) {
                if (err) {
                    console.log(err)
                }
                else {
                    console.dir(results._id)
                    sportExperienceId = results._id
                }
                callback();
            })
        },
        function (callback) {
            User.update(
                {_id: experience.userId},
                {
                    //$set:{
                    $push: {sportsExperience: sportExperienceId}
                    //}
                },
                {
                    upsert: true
                }, function (err, result) {

                    callback();
                })
        },
        function () {
            SportExperience.findOne(
                {_id: sportExperienceId}, function (err, expData) {

                    res.json(expData)

                })
        }
    ])


}


//
// function addExperienceOLD(req, res) {
//     var experience = req.body.experience;
//     var sportExperienceId;
//     async.series([
//         function (callback) {
//             if (typeof experience.refToTournament != 'undefined' && experience.refToTournament.length != 0) {
//                 Tournament.insertMany(experience.refToTournament, function (err, result) {
//                     if (err) {
//                         console.log(err)
//                     }
//                     else {
//                         Tournament.find({'userId': experience.userId}).lean().select('_id').exec(function (err, results) {
//                             if (err) {
//                                 console.log(err);
//                             }
//                             console.dir('ADDED TOURNAMENTS')
//                             experience.refToTournament = results;
//                             console.log(experience.refToTournament)
//                             callback();
//                         })
//                     }
//                 })
//             }
//             else {
//                 callback();
//             }
//
//         },
//         function (callback) {
//             if (typeof experience.refToAcademy != 'undefined' && experience.refToAcademy.length != 0) {
//                 Academy.insertMany(experience.refToAcademy, function (err, result) {
//                     if (err) {
//                         console.log(err)
//                     }
//                     else {
//                         Academy.find({'userId': experience.userId}).lean().select('_id').exec(function (err, results) {
//                             if (err) {
//                                 console.log(err);
//                             }
//                             experience.refToAcademy = results;
//                             callback();
//                         })
//                     }
//
//                 })
//             }
//             else {
//
//                 callback();
//             }
//         },
//         function (callback) {
//             var sportExperience = new SportExperience(experience);
//             sportExperience.save(function (err, results) {
//                 if (err) {
//                     console.log(err)
//                 }
//                 else {
//                     console.dir(results._id)
//                     sportExperienceId = results._id
//                 }
//                 callback();
//             })
//         },
//         function (callback) {
//             User.update(
//                 {_id: experience.userId},
//                 {
//                     //$set:{
//                     $push: {sportsExperience: sportExperienceId}
//                     //}
//                 },
//                 {
//                     upsert: true
//                 }, function (err, result) {
//
//                     callback();
//                 })
//         },
//         function () {
//             SportExperience.findOne(
//                 {_id: sportExperienceId}, function (err, expData) {
//
//                     res.json(expData)
//
//                 })
//         }
//     ])
//
//
// }


function findperson(req, res) {
    var sportId = req.query.sportId;
    var searchText = req.query.searchText;
    var sportExperienceIds = [];
    var sportExperience = [];
    var query;


    async.series([function (callback) {
        SportExperience
            .find({sportId: sportId})
            .lean().exec(function (err, result) {
            sportExperienceIds = _.map(result, '_id');
            console.log(result);
            if (searchText != '') {
                query = {
                    sportsExperience: {$in: sportExperienceIds},

                    $or: [
                        {firstName: {'$regex': searchText}},
                        {lastName: {'$regex': searchText}}]
                };
            }
            else {
                query = {
                    sportsExperience: {$in: sportExperienceIds}
                };
            }

            console.dir(query)
            callback();
        })
    }, function () {
        User.find(query).deepPopulate('sportsExperience').exec(function (err, result) {
            res.json(result);
        })
    }])


}
