var TagsLevel1 = require('../models/TagsLevel1');
var TagsLevel2 = require('../models/TagsLevel2');
var TagsLevel3 = require('../models/TagsLevel3');
var async = require('async');
var _ = require('underscore');
module.exports = {
    addTagsLevel1: addTagsLevel1,
    getTagsLevel1: getTagsLevel1,
    getAllLevelTags: getAllLevelTags
};

function addTagsLevel1(req, res) {
    var tagLevel1 = req.body.tagLevel1;
    var sportId = req.body.sportId;
    var tagsEntry = new TagsLevel1({
        tagLevel1: tagLevel1,
        sportId: sportId
    });

    tagsEntry.save(function (err, result) {
        console.log(result);
        res.json(result);
    })


}

function getTagsLevel1(req, res) {
    var sportId = req.query.sportId;
    TagsLevel1.find({sportId: sportId}).lean().exec(function (err, result) {
        res.json(result);
    })
}

function getAllLevelTags(req, res) {
    var tagsLevel1IdList = [];
    var tagsLevel2IdList = [];
    var tagsLevel3IdList = [];
    async.series([function (callback) {
        TagsLevel1.find().lean().exec(function (err, result) {
            _.each(result, function (obj) {
                tagsLevel1IdList.push(obj);
            })
            callback();
        })
    }, function (callback) {
        TagsLevel2.find({tagLevel1Id: {$in: _.pluck(tagsLevel1IdList,'_id')}}).lean().exec(function (err, result) {
            _.each(result, function (obj) {
                tagsLevel2IdList.push(obj);
            })
            callback();
        })

    }, function () {


        TagsLevel3.find({tagLevel2Id: {$in:  _.pluck(tagsLevel2IdList,'_id')}}).lean().exec(function (err, result) {
            _.each(result, function (obj) {
                tagsLevel3IdList.push(obj);
            })
            res.json({level1Tags:tagsLevel1IdList,level2Tags:tagsLevel2IdList,level3Tags:tagsLevel3IdList});
        })

    }])

}