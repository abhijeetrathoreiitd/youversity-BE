var Follow = require('../models/Follow');
var async = require('async')

module.exports = {
    addFollowing: addFollowing,
    getFollowers: getFollowers,
    unfollow:unfollow
};

function addFollowing(req, res) {
    var user = req.body.userId;
    var following = req.body.following;

    console.log('addFollowers');
    console.log(user);
    console.log(following);

    async.parallel([
        function (callback) {
            Follow.update({user: user}, {$push: {following: following}}, {upsert: false}, function (err, result) {
                callback();
            })
        }, function () {
            Follow.update({user: following}, {$push: {follower: user}}, {upsert: false}, function (err, result) {
                res.json(result)
            })
        }])
}



function unfollow(req, res) {
    var user = req.query.userId;
    var following = req.query.following;


    async.parallel([
        function (callback) {
            Follow.update({user: user}, {$pull: {following: following}}, {upsert: false}, function (err, result) {
                callback();
            })
        }, function () {
            Follow.update({user: following}, {$pull: {follower: user}}, {upsert: false}, function (err, result) {
                res.json(result)
            })
        }])
}



function getFollowers(req, res) {
    var user = req.params.userId;
    console.log('getFollowers');
    Follow.findOne({user: user}).lean().exec(function (err, result) {
        if (err)
            return err;
        res.json(result);
    })
}