var Package = require('../models/Package');


module.exports = {
    addPackage: addPackage,
    getPackage: getPackage,
    updateVideoInPackage: updateVideoInPackage
};
function addPackage(req, res) {
    var packageInfo = req.body.packageInfo;
    var package = new Package(packageInfo);
    package.save(function (err, results) {
        if (err) {
            console.log(err)
        }
        else
            res.json(results);
    })
}


function getPackage(req, res) {
    var packageBy = req.query.packageBy;
    var sportId = req.query.sportId;
    Package.find({
        packageBy: packageBy,
        sportId: sportId
    }).deepPopulate('refToVideos.videoBy').lean().exec(function (err, result) {
        res.json(result);
    })
}

function updateVideoInPackage(req, res) {
    var packageId = req.body.packageId;
    var videoList = req.body.videoList;
    console.log(videoList)


    Package.update({'_id': packageId}, {$set: {refToVideos: videoList}}, {upsert: false}, function (err, result) {
        res.json(result);
    })
}
