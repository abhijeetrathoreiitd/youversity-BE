var Subscribe = require('../models/Subscribe');
var async = require('async')

module.exports = {
    addSubscribed: addSubscribed,
    getSubscribedBy: getSubscribedBy
};

function addSubscribed(req, res) {
    var user = req.body.userId;
    var subscribed = req.body.subscribed;
    var verdict = req.body.verdict;
    console.log('++++')
    console.log(verdict)

    var queryObject1, queryObject2;
    if (!verdict) {
        console.log(1)
        queryObject1 = {$pull: {subscribed: subscribed}}
        queryObject2 = {$pull: {subscribedBy: user}}
    }
    else {
        queryObject1 = {$push: {subscribed: subscribed}}
        queryObject2 = {$push: {subscribedBy: user}}
    }

    async.series([
        function (callback) {

            Subscribe.update({user: user}, queryObject1, {upsert: false}, function (err, result) {
                callback();
            })
        }, function () {
            Subscribe.update({user: subscribed}, queryObject2, {upsert: false}, function (err, result) {
                res.json(result)
            })
        }])


}


function getSubscribedBy(req, res) {
    var user = req.params.userId;
    Subscribe.find({user: user}).select('subscribed')
        .lean()
        .exec(function (err, result) {
            res.json(result);
        })
}