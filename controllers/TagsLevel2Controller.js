var TagsLevel2 = require('../models/TagsLevel2');

module.exports = {
    addTagsLevel2: addTagsLevel2,
    getTagsLevel2:getTagsLevel2
};

function addTagsLevel2(req, res) {
    var tagLevel2 = req.body.tagLevel2;
    var tagLevel1Id = req.body.tagLevel1Id;
    var tagsEntry = new TagsLevel2({
        tagLevel1Id: tagLevel1Id,
        tagLevel2: tagLevel2
    });

    tagsEntry.save(function(err,result)
    {
        console.log(result);
        res.json(result);
    })
    

}


function getTagsLevel2(req,res)
{
    var tagLevel1Id=req.query.tagLevel1Id;
    TagsLevel2.find({tagLevel1Id:tagLevel1Id}).lean().exec(function(err,result)
    {
        res.json(result);
    })
}