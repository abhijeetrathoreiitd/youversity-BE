var VideoReview = require('../models/VideoReview');
var Video = require('../models/Video');
var _ = require('underscore');
var async = require('async');
var NewsFeedController=require('.././controllers/NewsFeedController');
module.exports = {
    addReview: addReview,
    getVideo: getVideo,
    submitReview: submitReview
};


function addReview(req, res) {
    var reviewInfo = req.body.review;
    console.dir(reviewInfo);

    VideoReview.findOne({videoId: reviewInfo.videoId}).lean().exec(function (err, result) {

        var reviewId;
        if (!result) {
            async.series([
                function (callback) {

                    var videoInfo = {
                        videoId: reviewInfo.videoId,
                        videoBy: reviewInfo.videoBy,
                        review: {
                            coachId: reviewInfo.coachId
                        }
                    }
                    console.dir(videoInfo);
                    var video = new VideoReview(videoInfo);
                    video.save(function (err, results) {
                        if (err) {
                            console.log(err);
                        }
                        else {
                            reviewId = results._id;
                            callback();
                        }
                    })
                },
                function () {
                    console.log(reviewId);

                    Video.update({_id: reviewInfo.videoId}, {$set: {review: reviewId}}, {upsert: false}, function (err, result) {
                        res.json(result);
                    })

                }
            ])
        }
        else {
            if (result.review.length < 2) {
                reviewId = result._id;
                VideoReview.update({_id: reviewId}, {$push: {review: {coachId: reviewInfo.coachId}}}, {upsert: false}, function (err, result) {
                    res.json(result);
                })
            }
            else {

                console.log(456)
                res.json({err: 'video already send for review'});
            }


        }
    })
}
function getVideo(req, res) {
    var videoIdArray;
    var coachId = req.body.coachId;
    var lastVideoFetchTime = req.body.lastVideoFetchTime;
    var newTime;
    console.log('--------------')
    console.dir(req.body);
    if (lastVideoFetchTime != '')
        var queryObject = {'review.coachId': coachId, createdAt: {$gte: lastVideoFetchTime}};
    else
        var queryObject = {'review.coachId': coachId};

    console.dir(queryObject);

    async.series([
        function (callback) {
            VideoReview.find(queryObject).select('videoId').lean().exec(function (err, videos) {
                videoIdArray = _.pluck(videos, 'videoId');
                newTime = new Date();
                callback();
            })
        },
        function () {
            Video.find({_id: {$in: videoIdArray}}).deepPopulate('videoBy review').lean().exec(function (err, result) {
                res.json({videoList: result, lastVideoFetchTime: newTime});
            })
        }
    ])
}
function submitReview(req, res) {
    var videoId = req.body.videoId;
    var coachId = req.body.coachId;
    var review = req.body.review;
    var audioUrl = req.body.audioUrl;


    var reviewObject = {};
    async.series([
        function (callback) {
            VideoReview.find({'videoId': videoId}).lean().select('review').exec(function (err, result) {
                console.log(err)
                reviewObject = _.pluck(result, 'review')[0];
                _.each(reviewObject, function (rev) {
                    if (rev.coachId == coachId) {
                        if(typeof review!='undefined')
                        rev.review = review;
                        if(typeof audioUrl!='undefined')
                        rev.audioUrl = audioUrl;
                    }
                })
                console.log('review Object after each');
                console.log(reviewObject);
                callback();
            })

        },
        function () {
            VideoReview.update({'videoId': videoId}, {$set: {'review':reviewObject}}, {upsert: true}, function (err, result) {
                NewsFeedController.addNewsFeed({userId:coachId,videoId:videoId,newsFeedCode:11});
                res.json(result);
            })
        }])

}

function myReviews(req, res) {
    var query = {
        coachId: req.body.coachId,
        review: {$exists: true}
    };


    VideoReview.find(query).lean().exec(function (err, result) {
        res.json(result);
    })
}

