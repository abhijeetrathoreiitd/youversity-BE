var Tags = require('../models/Tags');
var Sports = require('../models/Sports');

module.exports = {
    addTags: addTags
};

function addTags(req, res) {
    var tagName = req.body.tagName;
    var sportId = req.body.sportId;
    var tagsEntry = new Tags({
        tagName: tagName,
        sportId: sportId
    });
    tagsEntry.save(function (err, tagsDetails) {
        if (err) {

            console.log(err)
        }
        else {
            Sports.update(
                {_id: sportId},
                {
                    $push: {refToTags: tagsDetails._id}
                },
                {
                    upsert: true
                }, function (err, result) {

                    res.json(tagsDetails)
                })
        }
    })
}