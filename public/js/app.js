'use strict';
// Declare app level module which depends on filters, and services

var app = angular.module('myApp', [
    'ngRoute', 'angular.filter'
])

app.config(function ($routeProvider, $locationProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.interceptors.push(function ($q, $location) {
        return {
            'responseError': function (rejection) {
                if (rejection.status == 302) {
                    if (JSON.parse(rejection.data).redirect == 'adminhome') {
                        $location.path('/admin');
                    }
                    else if (JSON.parse(rejection.data).redirect == 'userhome') {
                        $location.path('/user');
                    }
                    else if (JSON.parse(rejection.data).redirect == 'signup') {
                        $location.path('/signup');
                    }
                }
                return $q.reject(rejection);
            }
        };
    });

    $routeProvider.when('/addCategory', {
        templateUrl: 'partials/addCategory',
        controller: 'addCategoryCtrl',
        controllerAs: 'category'
    }).when('/addService', {
        templateUrl: 'partials/addService',
        controller: 'addServiceCtrl',
        controllerAs: 'service'
    }).otherwise({
        redirectTo: '/addCategory'
    });
    $locationProvider.html5Mode(true);
})
    .run(function ($http, $rootScope) {


    })
    .controller('addCategoryCtrl', addCategoryCtrl)
    .controller('addServiceCtrl', addServiceCtrl)
    .controller('headerCtrl', function ($scope, $http, $location, $rootScope) {
        $scope.logout = function () {
            $http.get('/logout').then(function (data) {
                //on successful clears the information of user from $rootScope.loggedInUser variable
                $rootScope.loggedInUser = undefined;
                $location.path('/login');
            })
        }
    })


function addCategoryCtrl($http) {
    var category = this;
    category.newCategory = {};

    category.addCategory = function () {

        console.log(category.newCategory.category)
        $http.post('/category', {
            category: category.newCategory.category
        }).then(function (data) {
        }, function (err) {
            console.log(err);
        })
    }
}


function addServiceCtrl($http) {


    $http.get('/category').then(function (data) {
        console.log(data);
    }, function (err) {
        console.log(err);
    })


    var service = this;
    service.newService = {};

    service.addService = function () {
        $http.post('/service', {
            service: service.newService.service,
            category_type_id: service.newService.category_type_id
        }).then(function (data) {
        }, function (err) {
            console.log(err);
        })
    }
}


